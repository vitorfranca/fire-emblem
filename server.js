var http = require('http'),
	path = require('path'),
	express = require('express'),
	app = express();

var oneDay = 1000 * 60 * 60 * 24;

app.use('/img',  express.static(__dirname + '/app/public/img', { maxAge: oneDay }));
app.use('/js',   express.static(__dirname + '/app/public/js'));
app.use('/css',  express.static(__dirname + '/app/public/css'));
app.use('/html', express.static(__dirname + '/app/html'));

app.all('/*', function(req, res, next) {
    res.sendFile('index.html', { root: __dirname+'/app' });
});

var server = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('app listening at http://%s:%s', host, port);
});
