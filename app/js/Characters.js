app.factory('Characters', function () {
    return [
        {   "name": "Chrom",
            "startingLevel": 1,
            "baseStats": {
                "hp":  20,
                "str": 7,
                "mag": 1,
                "skl": 8,
                "spd": 8,
                "lck": 5,
                "def": 7,
                "res": 1,
                "mov": 6
            },
            "modifiers": {
                "str": 1,
                "mag": 0,
                "skl": 1,
                "spd": 0,
                "lck": 1,
                "def": -1,
                "res": -1
            },
            "growthRates": {
                "hp":  0.4,
                "str": 0.4,
                "mag": 0.35,
                "skl": 0.35,
                "spd": 0.35,
                "lck": 0.55,
                "def": 0.3,
                "res": 0.2
            },
            "supports": [
                "Robin",
                "Fredrick",
                "Lissa",
                "Sully",
                "Vaike",
                "Sumia",
                "Maribelle",
                "Gaius",
                "Olivia"
            ],
            "marriable": [
                "Robin",
                "Sully",
                "Sumia",
                "Maribelle",
                "Olivia"
            ],
            "initialClass": "Lord",
            "children": [
                "Lucina"
            ],
            "sex": "Male",
            "classSet": [
                "Lord",
                "Cavalier",
                "Archer"
            ]
        },
        {   "name": "Robin",
            "startingLevel": 1,
            "baseStats": {
                "hp":  19,
                "str": 6,
                "mag": 5,
                "skl": 5,
                "spd": 6,
                "lck": 4,
                "def": 6,
                "res": 4,
                "mov": 5
            },
            "initialClass": "Tactician",
            "children": [
                "Morgan"
            ]
        },
        {   "name": "Lissa",
            "startingLevel": 1,
            "baseStats": {
                "hp":  17,
                "str": 1,
                "mag": 5,
                "skl": 4,
                "spd": 4,
                "lck": 8,
                "def": 3,
                "res": 4,
                "mov": 5
            },
            "initialClass": "Cleric",
            "children": [
                "Owain"
            ],
            "sex": "Female",
            "classSet": [
                "Cleric",
                "Pegasus Knight",
                "Troubadour"
            ]
        },
        {   "name": "Fredrick",
            "startingLevel": 1,
            "baseStats": {
                "hp":  28,
                "str": 13,
                "mag": 2,
                "skl": 12,
                "spd": 10,
                "lck": 6,
                "def": 14,
                "res": 3,
                "mov": 7
            },
            "initialClass": "Great Knight",
            "sex": "Male",
            "classSet": [
                "Cavalier",
                "Knight",
                "Wyvern Rider"
            ]
        },
        {   "name": "Sully",
            "startingLevel": 2,
            "baseStats": {
                "hp":  20,
                "str": 7,
                "mag": 1,
                "skl": 8,
                "spd": 8,
                "lck": 6,
                "def": 7,
                "res": 2,
                "mov": 7
            },
            "initialClass": "Cavalier",
            "children": [
                "Kjelle"
            ],
            "sex": "Female",
            "classSet": [
                "Cavalier",
                "Myrmidon",
                "Wyvern Rider"
            ]
        },
        {   "name": "Virion",
            "startingLevel": 2,
            "baseStats": {
                "hp":  19,
                "str": 6,
                "mag": 0,
                "skl": 9,
                "spd": 5,
                "lck": 7,
                "def": 6,
                "res": 1,
                "mov": 5
            },
            "initialClass": "Archer",
            "sex": "Male",
            "classSet": [
                "Archer",
                "Myrmidon",
                "Wyvern Rider"
            ]
        },
        {   "name": "Stahl",
            "startingLevel": 2,
            "baseStats": {
                "hp":  22,
                "str": 8,
                "mag": 0,
                "skl": 7,
                "spd": 6,
                "lck": 5,
                "def": 8,
                "res": 1,
                "mov": 7
            },
            "initialClass": "Cavalier",
            "sex": "Male",
            "classSet": [
                "Cavalier",
                "Archer",
                "Myrmidon"
            ]
        },
        {   "name": "Vaike",
            "startingLevel": 3,
            "baseStats": {
                "hp":  24,
                "str": 9,
                "mag": 0,
                "skl": 8,
                "spd": 6,
                "lck": 4,
                "def": 5,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Fighter",
            "sex": "Male",
            "classSet": [
                "Fighter",
                "Thief",
                "Barbarian"
            ]
        },
        {   "name": "Miriel",
            "startingLevel": 1,
            "baseStats": {
                "hp":  18,
                "str": 0,
                "mag": 6,
                "skl": 5,
                "spd": 7,
                "lck": 6,
                "def": 3,
                "res": 4,
                "mov": 5
            },
            "initialClass": "Mage",
            "children": [
                "Laurent"
            ],
            "sex": "Female",
            "classSet": [
                "Mage",
                "Troubadour",
                "Dark Mage"
            ]
        },
        {   "name": "Sumia",
            "startingLevel": 1,
            "baseStats": {
                "hp":  18,
                "str": 6,
                "mag": 3,
                "skl": 11,
                "spd": 11,
                "lck": 8,
                "def": 5,
                "res": 7,
                "mov": 7
            },
            "initialClass": "Pegasus Knight",
            "children": [
                "Cynthia"
            ],
            "sex": "Female",
            "classSet": [
                "Pegasus Knight",
                "Knight",
                "Cleric"
            ]
        },
        {   "name": "Kellam",
            "startingLevel": 5,
            "baseStats": {
                "hp":  21,
                "str": 10,
                "mag": 0,
                "skl": 7,
                "spd": 5,
                "lck": 3,
                "def": 12,
                "res": 2,
                "mov": 4
            },
            "initialClass": "Knight",
            "sex": "Male",
            "classSet": [
                "Knight",
                "Thief",
                "Priest"
            ]
        },
        {   "name": "Donnel",
            "startingLevel": 1,
            "baseStats": {
                "hp":  16,
                "str": 4,
                "mag": 0,
                "skl": 2,
                "spd": 3,
                "lck": 11,
                "def": 3,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Villager",
            "sex": "Male",
            "classSet": [
                "Villager",
                "Fighter",
                "Mercenary"
            ]
        },
        {   "name": "Lon'qu",
            "startingLevel": 4,
            "baseStats": {
                "hp":  20,
                "str": 6,
                "mag": 1,
                "skl": 12,
                "spd": 13,
                "lck": 7,
                "def": 7,
                "res": 2,
                "mov": 5
            },
            "initialClass": "Myrmidon",
            "sex": "Male",
            "classSet": [
                "Myrmidon",
                "Thief",
                "Wyvern Rider"
            ]
        },
        {   "name": "Ricken",
            "startingLevel": 3,
            "baseStats": {
                "hp":  20,
                "str": 3,
                "mag": 8,
                "skl": 6,
                "spd": 5,
                "lck": 10,
                "def": 6,
                "res": 3,
                "mov": 5
            },
            "initialClass": "Mage",
            "sex": "Male",
            "classSet": [
                "Mage",
                "Cavalier",
                "Archer"
            ]
        },
        {   "name": "Maribelle",
            "startingLevel": 3,
            "baseStats": {
                "hp":  18,
                "str": 0,
                "mag": 5,
                "skl": 4,
                "spd": 6,
                "lck": 5,
                "def": 3,
                "res": 6,
                "mov": 7
            },
            "initialClass": "Troubadour",
            "children": [
                "Brady"
            ],
            "sex": "Female",
            "classSet": [
                "Troubadour",
                "Pegasus Knight",
                "Mage"
            ]
        },
        {   "name": "Gaius",
            "startingLevel": 5,
            "baseStats": {
                "hp":  22,
                "str": 7,
                "mag": 0,
                "skl": 13,
                "spd": 15,
                "lck": 6,
                "def": 5,
                "res": 2,
                "mov": 5
            },
            "initialClass": "Thief",
            "sex": "Male",
            "classSet": [
                "Thief",
                "Fighter",
                "Myrmidon"
            ]
        },
        {   "name": "Panne",
            "startingLevel": 6,
            "baseStats": {
                "hp":  28,
                "str": 8,
                "mag": 1,
                "skl": 9,
                "spd": 10,
                "lck": 8,
                "def": 7,
                "res": 3,
                "mov": 6
            },
            "initialClass": "Taguel",
            "children": [
                "Yarne"
            ],
            "sex": "Female",
            "classSet": [
                "Taguel",
                "Wyvern Rider",
                "Thief"
            ]
        },
        {   "name": "Cordelia",
            "startingLevel": 7,
            "baseStats": {
                "hp":  25,
                "str": 9,
                "mag": 3,
                "skl": 13,
                "spd": 12,
                "lck": 9,
                "def": 8,
                "res": 8,
                "mov": 7
            },
            "initialClass": "Pegasus Knight",
            "children": [
                "Severa"
            ],
            "sex": "Female",
            "classSet": [
                "Pegasus Knight",
                "Mercenary",
                "Dark Mage"
            ]
        },
        {   "name": "Gregor",
            "startingLevel": 10,
            "baseStats": {
                "hp":  30,
                "str": 12,
                "mag": 0,
                "skl": 13,
                "spd": 11,
                "lck": 8,
                "def": 10,
                "res": 2,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  31,
                "str": 13,
                "mag": 0,
                "skl": 14,
                "spd": 12,
                "lck": 8,
                "def": 11,
                "res": 2,
                "mov": 5
            },
            "initialClass": "Mercenary",
            "sex": "Male",
            "classSet": [
                "Mercenary",
                "Barbarian",
                "Myrmidon"
            ]
        },
        {   "name": "Nowi",
            "startingLevel": 3,
            "baseStats": {
                "hp":  18,
                "str": 4,
                "mag": 0,
                "skl": 2,
                "spd": 3,
                "lck": 8,
                "def": 2,
                "res": 2,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  19,
                "str": 5,
                "mag": 0,
                "skl": 3,
                "spd": 4,
                "lck": 9,
                "def": 3,
                "res": 3,
                "mov": 6
            },
            "initialClass": "Manakete",
            "children": [
                "Nah"
            ],
            "sex": "Female",
            "classSet": [
                "Manakete",
                "Mage",
                "Wyvern Rider"
            ]
        },
        {   "name": "Libra",
            "startingLevel": 1,
            "baseStats": {
                "hp":  38,
                "str": 14,
                "mag": 15,
                "skl": 13,
                "spd": 13,
                "lck": 10,
                "def": 11,
                "res": 16,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  39,
                "str": 14,
                "mag": 16,
                "skl": 13,
                "spd": 14,
                "lck": 10,
                "def": 11,
                "res": 16,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  40,
                "str": 15,
                "mag": 16,
                "skl": 13,
                "spd": 14,
                "lck": 11,
                "def": 12,
                "res": 17,
                "mov": 6
            },
            "initialClass": "War Monk",
            "sex": "Male",
            "classSet": [
                "Priest",
                "Mage",
                "Dark Mage"
            ]
        },
        {   "name": "Tharja",
            "startingLevel": 10,
            "baseStats": {
                "hp":  25,
                "str": 4,
                "mag": 11,
                "skl": 5,
                "spd": 12,
                "lck": 3,
                "def": 10,
                "res": 7,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  26,
                "str": 4,
                "mag": 12,
                "skl": 5,
                "spd": 13,
                "lck": 3,
                "def": 10,
                "res": 7,
                "mov": 5
            },
            "initialClass": "Dark Mage",
            "children": [
                "Noire"
            ],
            "sex": "Female",
            "classSet": [
                "Dark Mage",
                "Knight",
                "Archer"
            ]
        },
        {   "name": "Anna",
            "startingLevel": 1,
            "baseStats": {
                "hp":  35,
                "str": 12,
                "mag": 17,
                "skl": 22,
                "spd": 21,
                "lck": 25,
                "def": 8,
                "res": 10,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  36,
                "str": 12,
                "mag": 17,
                "skl": 23,
                "spd": 22,
                "lck": 26,
                "def": 8,
                "res": 10,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  37,
                "str": 13,
                "mag": 18,
                "skl": 23,
                "spd": 22,
                "lck": 27,
                "def": 9,
                "res": 11,
                "mov": 6
            },
            "initialClass": "Trickster",
            "sex": "Female",
            "classSet": [
                "Thief",
                "Archer",
                "Mage"
            ]
        },
        {   "name": "Olivia",
            "startingLevel": 1,
            "baseStats": {
                "hp":  18,
                "str": 3,
                "mag": 1,
                "skl": 8,
                "spd": 9,
                "lck": 5,
                "def": 3,
                "res": 2,
                "mov": 5
            },
            "initialClass": "Dancer",
            "children": [
                "Inigo"
            ],
            "sex": "Female",
            "classSet": [
                "Dancer",
                "Myrmidon",
                "Pegasus Knight"
            ]
        },
        {   "name": "Cherche",
            "startingLevel": 12,
            "baseStats": {
                "hp":  30,
                "str": 14,
                "mag": 1,
                "skl": 12,
                "spd": 11,
                "lck": 8,
                "def": 15,
                "res": 2,
                "mov": 7
            },
            "baseStatsH": {
                "hp":  31,
                "str": 15,
                "mag": 1,
                "skl": 13,
                "spd": 12,
                "lck": 9,
                "def": 16,
                "res": 2,
                "mov": 7
            },
            "baseStatsL": {
                "hp":  33,
                "str": 16,
                "mag": 2,
                "skl": 14,
                "spd": 13,
                "lck": 10,
                "def": 17,
                "res": 2,
                "mov": 7
            },
            "initialClass": "Wyvern Rider",
            "children": [
                "Gerome"
            ],
            "sex": "Female",
            "classSet": [
                "Wyvern Rider",
                "Troubadour",
                "Cleric"
            ]
        },
        {   "name": "Henry",
            "startingLevel": 12,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Dark Mage",
            "sex": "Male",
            "classSet": [
                "Dark Mage",
                "Barbarian",
                "Thief"
            ]
        },
        {   "name": "Say'ri",
            "startingLevel": 1,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Swordmaster",
            "sex": "Female",
            "classSet": [
                "Myrmidon",
                "Pegasus Knight",
                "Wyvern Rider"
            ]
        },
        {   "name": "Tiki",
            "startingLevel": 20,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Manakete",
            "sex": "Female",
            "classSet": [
                "Manakete",
                "Wyvern Rider",
                "Mage"
            ]
        },
        {   "name": "Basilio",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Warrior",
            "sex": "Male",
            "classSet": [
                "Fighter",
                "Barbarian",
                "Knight"
            ]
        },
        {   "name": "Flavia",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Hero",
            "sex": "Female",
            "classSet": [
                "Mercenary",
                "Thief",
                "Knight"
            ]
        },
        {   "name": "Lucina",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Lord",
            "sex": "Female"
        },
        {   "name": "Morgan",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            }
        },
        {   "name": "Owain",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Myrmidon",
            "sex": "Male"
        },
        {   "name": "Inigo",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Mercenary",
            "sex": "Male"
        },
        {   "name": "Brady",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Priest",
            "sex": "Male"
        },
        {   "name": "Kjelle",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 4
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 4
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 4
            },
            "initialClass": "Knight",
            "sex": "Female"
        },
        {   "name": "Cynthia",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "initialClass": "Pegasus Knight",
            "sex": "Female"
        },
        {   "name": "Severa",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Mercenary",
            "sex": "Female"
        },
        {   "name": "Gerome",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 8
            },
            "initialClass": "Wyvern Rider",
            "sex": "Male"
        },
        {   "name": "Yarne",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Taguel",
            "sex": "Male"
        },
        {   "name": "Laurent",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Mage",
            "sex": "Male"
        },
        {   "name": "Noire",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 5
            },
            "initialClass": "Archer",
            "sex": "Female"
        },
        {   "name": "Nah",
            "startingLevel": 10,
            "baseStats": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsH": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "baseStatsL": {
                "hp":  0,
                "str": 0,
                "mag": 0,
                "skl": 0,
                "spd": 0,
                "lck": 0,
                "def": 0,
                "res": 0,
                "mov": 6
            },
            "initialClass": "Manakete",
            "sex": "Female"
        }
    ];
});
