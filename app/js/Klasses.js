app.factory('Klasses', function () {
    return [
        {
            "name": "Lord",
            "promote":      [ "Great Lord" ],
            "baseStats":    { "hp" : 18, "str": 6, "mag": 0, "skl": 5, "spd": 7, "def": 7, "res": 0, "mov": 5 },
            "maxStats":     { "hp" : 60, "str": 25, "mag": 20, "skl": 26, "spd": 28, "lck": 30, "def": 25, "res": 25 },
            "growthRates":  { "hp" : 0.4, "str": 0.2, "mag": 0, "skl": 0.2, "spd": 0.2, "def": 0.1, "res": 0.02 },
            "weapons":      [ "Sword" ]
        },
        {
            "name": "Great Lord",
            "baseStats":    { "hp" : 23, "str": 10, "mag": 0, "skl": 7, "spd": 9, "def": 10, "res": 3, "mov": 6 },
            "maxStats":     { "hp" : 80, "str": 43, "mag": 30, "skl": 40, "spd": 41, "lck": 45, "def": 42, "res": 40 },
            "growthRates":  { "hp" : 0.4, "str": 0.2, "mag": 0, "skl": 0.2, "spd": 0.2, "def": 0.1, "res": 0.02 },
            "weapons":      [ "Sword", "Lance" ]
        },
        {
            "name": "Tactician",
            "promote":      [ "Grandmaster" ],
            "baseStats":    { "hp" : 16, "str": 4, "mag": 3, "skl": 5, "spd": 5, "def": 5, "res": 3, "mov": 5 },
            "maxStats":     { "hp" : 60, "str": 25, "mag": 25, "skl": 25, "spd": 25, "lck": 30, "def": 25, "res": 25 },
            "growthRates":  { "hp" : 0.4, "str": 0.15, "mag": 0.15, "skl": 0.15, "spd": 0.15, "def": 0.1, "res": 0.1 },
            "weapons":      [ "Sword", "Tome" ]
        },
        {
            "name": "Grandmaster",
            "baseStats":    { "hp" : 20, "str": 7, "mag": 6, "skl": 7, "spd": 7, "def": 7, "res": 5, "mov": 6 },
            "maxStats":     { "hp" : 80, "str": 40, "mag": 40, "skl": 40, "spd": 40, "lck": 45, "def": 40, "res": 40 },
            "growthRates":  { "hp" : 0.4, "str": 0.15, "mag": 0.15, "skl": 0.15, "spd": 0.15, "def": 0.1, "res": 0.1 },
            "weapons":      [ "Sword", "Tome" ]
        },
        {
            "name": "Cavalier",
            "promote":      [ "Paladin", "Great Knight" ],
            "baseStats":    { "hp" : 18, "str": 6, "mag": 0, "skl": 5, "spd": 6, "def": 7, "res": 0, "mov": 7 },
            "maxStats":     { "hp" : 60, "str": 26, "mag": 20, "skl": 25, "spd": 25, "lck": 30, "def": 26, "res": 26 },
            "growthRates":  { "hp" : 0.45, "str": 0.2, "mag": 0, "skl": 0.2, "spd": 0.2, "def": 0.1, "res": 0.05 },
            "weapons":      [ "Sword", "Lance" ]
        },
        {
            "name": "Knight",
            "promote":      [ "Great Knight", "General" ],
            "baseStats":    { "hp" : 18, "str": 8, "mag": 0, "skl": 4, "spd": 2, "def": 11, "res": 0, "mov": 4 },
            "maxStats":     { "hp" : 60, "str": 30, "mag": 20, "skl": 26, "spd": 23, "lck": 30, "def": 30, "res": 22 },
            "growthRates":  { "hp" : 0.5, "str": 0.25, "mag": 0, "skl": 0.15, "spd": 0.1, "def": 0.15, "res": 0.05 },
            "weapons":      [ "Lance" ]
        },
        {
            "name": "Paladin",
            "baseStats":    { "hp" : 25, "str": 9, "mag": 1, "skl": 7, "spd": 8, "def": 10, "res": 6, "mov": 8 },
            "maxStats":     { "hp" : 80, "str": 42, "mag": 30, "skl": 40, "spd": 40, "lck": 45, "def": 42, "res": 42 },
            "growthRates":  { "hp" : 0.45, "str": 0.2, "mag": 0, "skl": 0.2, "spd": 0.2, "def": 0.1, "res": 0.1 },
            "weapons":      [ "Sword", "Lance" ]
        },
        {
            "name": "Great Knight",
            "baseStats":    { "hp" : 26, "str": 11, "mag": 0, "skl": 6, "spd": 5, "def": 14, "res": 1, "mov": 7 },
            "maxStats":     { "hp" : 80, "str": 48, "mag": 20, "skl": 34, "spd": 37, "lck": 45, "def": 48, "res": 30 },
            "growthRates":  { "hp" : 0.5, "str": 0.25, "mag": 0, "skl": 0.15, "spd": 0.15, "def": 0.15, "res": 0.05 },
            "weapons":      [ "Sword", "Lance", "Axe" ]
        },
        {
            "name": "General",
            "baseStats":    { "hp" : 28, "str": 12, "mag": 0, "skl": 7, "spd": 4, "def": 15, "res": 3, "mov": 5 },
            "maxStats":     { "hp" : 80, "str": 50, "mag": 30, "skl": 41, "spd": 35, "lck": 45, "def": 50, "res": 35 },
            "growthRates":  { "hp" : 0.5, "str": 0.25, "mag": 0, "skl": 0.15, "spd": 0.1, "def": 0.15, "res": 0.1 },
            "weapons":      [ "Lance", "Axe" ]
        },
        {
            "name": "Myrmidon",
            "promote": [ "Swordmaster", "Assassin" ],
            "weapons": [ "Sword" ]
        },
        {
            "name": "Thief",
            "promote": [ "Assassin", "Trickster" ],
            "weapons": [ "Sword" ]
        },
        {
            "name": "Swordmaster",
            "weapons": [ "Sword" ]
        },
        {
            "name": "Assassin",
            "weapons": [ "Sword", "Bow" ]
        },
        {
            "name": "Trickster",
            "weapons": [ "Sword", "Staff" ]
        },
        {
            "name": "Barbarian",
            "promote": [ "Berserker", "Warrior" ],
            "weapons": [ "Axe" ]
        },
        {
            "name": "Fighter",
            "promote": [ "Warrior", "Hero" ],
            "weapons": [ "Axe" ]
        },
        {
            "name": "Mercenary",
            "promote": [ "Hero", "Bow Knight" ],
            "weapons": [ "Sword" ]
        },
        {
            "name": "Archer",
            "promote":      [ "Bow Knight", "Sniper" ],
            "baseStats":    { "hp" : 16, "str": 5, "mag": 0, "skl": 8, "spd": 6, "def": 5, "res": 0, "mov": 5 },
            "maxStats":     { "hp" : 60, "str": 26, "mag": 20, "skl": 29, "spd": 25, "lck": 30, "def": 25, "res": 21 },
            "growthRates":  { "hp" : 0.45, "str": 0.15, "mag": 0, "skl": 0.3, "spd": 0.15, "def": 0.1, "res": 0.05 },
            "weapons":      [ "Bow" ]
        },
        {
            "name": "Berserker",
            "weapons": [ "Axe" ]
        },
        {
            "name": "Warrior",
            "weapons": [ "Axe", "Bow" ]
        },
        {
            "name": "Hero",
            "weapons": [ "Sword", "Axe" ]
        },
        {
            "name": "Bow Knight",
            "baseStats":    { "hp" : 24, "str": 8, "mag": 0, "skl": 10, "spd": 10, "def": 6, "res": 2, "mov": 8 },
            "maxStats":     { "hp" : 80, "str": 40, "mag": 30, "skl": 43, "spd": 41, "lck": 45, "def": 35, "res": 30 },
            "growthRates":  { "hp" : 0.5, "str": 0.2, "mag": 0, "skl": 0.25, "spd": 0.2, "def": 0.05, "res": 0.05 },
            "weapons":      [ "Bow", "Sword" ]
        },
        {
            "name": "Sniper",
            "baseStats":    { "hp" : 20, "str": 7, "mag": 1, "skl": 12, "spd": 9, "def": 10, "res": 3, "mov": 6 },
            "maxStats":     { "hp" : 80, "str": 41, "mag": 30, "skl": 48, "spd": 40, "lck": 45, "def": 40, "res": 31 },
            "growthRates":  { "hp" : 0.45, "str": 0.15, "mag": 0, "skl": 0.3, "spd": 0.15, "def": 0.15, "res": 0.05 },
            "weapons":      [ "Bow" ]
        },
        {
            "name": "Pegasus Knight",
            "promote": [ "Falcon Knight", "Dark Flier" ],
            "weapons": [ "Lance" ]
        },
        {
            "name": "Wyvern Rider",
            "promote": [ "Wyvern Lord", "Griffon Rider" ],
            "weapons": [ "Axe" ]
        },
        {
            "name": "Falcon Knight",
            "weapons": [ "Lance", "Staff" ]
        },
        {
            "name": "Dark Flier",
            "weapons": [ "Lance", "Tome" ]
        },
        {
            "name": "Wyvern Lord",
            "weapons": [ "Axe", "Lance" ]
        },
        {
            "name": "Griffon Rider",
            "weapons": [ "Axe" ]
        },
        {
            "name": "Dark Mage",
            "promote": [ "Sorcerer", "Dark Knight" ],
            "weapons": [ "Tome", "Dark" ]
        },
        {
            "name": "Mage",
            "promote": [ "Dark Knight", "Sage" ],
            "weapons": [ "Tome" ]
        },
        {
            "name": "Priest",
            "promote": [ "Sage", "War Monk" ],
            "weapons": [ "Staff" ]
        },
        {
            "name": "Cleric",
            "promote": [ "Sage", "War Cleric" ],
            "weapons": [ "Staff" ]
        },
        {
            "name": "Troubadour",
            "promote": [ "War Monk", "Valkyrie" ],
            "weapons": [ "Staff" ]
        },
        {
            "name": "Sorcerer",
            "weapons": [ "Tome", "Dark" ]
        },
        {
            "name": "Dark Knight",
            "weapons": [ "Tome", "Sword" ]
        },
        {
            "name": "Sage",
            "weapons": [ "Tome", "Staff" ]
        },
        {
            "name": "War Monk",
            "weapons": [ "Staff", "Axe" ]
        },
        {
            "name": "War Cleric",
            "weapons": [ "Staff", "Axe" ]
        },
        {
            "name": "Valkyrie",
            "weapons": [ "Tome", "Staff" ]
        },
        {
            "name": "Villager",
            "weapons": [ "Lance" ]
        },
        {
            "name": "Dancer",
            "weapons": [ "Sword" ]
        },
        {
            "name": "Taguel",
            "weapons": [ "Stone" ]
        },
        {
            "name": "Manakete",
            "weapons": [ "Stone" ]
        },
        {
            "name": "Dread Fighter",
            "weapons": [ "Sword", "Axe", "Tome" ]
        },
        {
            "name": "Bride",
            "weapons": [ "Lance", "Bow", "Staff" ]
        },
        {
            "name": "Lodestar",
            "weapons": [ "Sword" ]
        }
    ];
});
