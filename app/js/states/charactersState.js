app.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('home.characters', {
    abstract: true,
    url: "/characters",
    template: "<div ui-view></div>",
  })

  .state('home.characters.list', {
    url: '/list',
    templateUrl: "html/characters.html",
    controller: 'charactersStateCtrl'
  })

  .state('home.characters.details', {
    url:'/:name',
    templateUrl: "html/characterState.html",
    resolve: {
      character: function (Characters, $stateParams) {
        return _(Characters).find({ name: $stateParams.name });
      }
    },
    controller: 'characterStateCtrl'
  });
})
.controller('charactersStateCtrl', function ($scope, Characters) {
  $scope.characters = Characters;
})
.controller('characterStateCtrl', function ($scope, character, Characters, Skills, Klasses) {
  $scope.character = character;

  var allKlasses = _.chain(character.classSet).map(function (klass) {
    return _(Klasses).find({ name: klass }).promote;
  }).flatten().value();

  character.skills = _(Skills).filter(function (skill) {
    return _(allKlasses).includes(skill.klass);
  });
});
