app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home.klasses', {
        abstract: true,
        url: "/classes",
        template: "<div ui-view></div>"
    })

    .state('home.klasses.list', {
        url: '/list',
        templateUrl: "html/klasses.html",
        controller: 'klassesStateCtrl'
    })

    .state('home.klasses.details', {
      url:'/:name',
      templateUrl: "html/klassState.html",
      resolve: {
        klass: function (Klasses, $stateParams) {
          return _(Klasses).find({ name: $stateParams.name });
        }
      },
      controller: 'klassStateCtrl'
    });
})
.controller('klassesStateCtrl', function ($scope, Klasses) {
    $scope.klasses = Klasses;
})
.controller('klassStateCtrl', function ($scope, klass, Skills, Klasses) {
    $scope.klass = klass;
    $scope.klass.skills = _(Skills).where({ klass: klass.name });
    $scope.klass.promoteFrom = _(Klasses).filter(function (kl) {
        return _(kl.promote).includes(klass.name);
    });
});
