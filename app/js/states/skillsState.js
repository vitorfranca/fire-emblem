app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home.skills', {
        abstract: true,
        url: '/skills',
        template: "<div ui-view></div>",
    })

    .state('home.skills.list', {
        url: '/list',
        templateUrl: "html/skills.html",
        controller: 'skillsStateCtrl'
    })

    .state('home.skills.details', {
      url:'/:name',
      templateUrl: "html/skillState.html",
      resolve: {
        skill: function (Skills, $stateParams) {
          return _(Skills).find({ name: $stateParams.name });
        }
      },
      controller: function ($scope, skill) { $scope.skill = skill; }
    });
})
.controller('skillsStateCtrl', function ($scope, Skills) {
    $scope.skills = Skills;
});
