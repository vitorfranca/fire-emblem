app.directive('character', function ($window, $timeout) {
    return {
        restrict: 'AE',
        templateUrl: 'html/character.html',
        scope: {},
        link: function ($scope, $elem, $attr) {
            $scope.character = angular.fromJson($attr.character);
        },
        controller: 'characterCtrl'
    };
})
.controller('characterCtrl', function ($scope, $mdDialog) {
    $scope.showDetails = function(ev) {
        console.log($scope.character);
        $mdDialog.show({
            controller: 'characterDetailsCtrl',
            templateUrl: 'html/characterDetails.html',
            targetEvent: ev,
            locals: {
                character: $scope.character
            }
        });
    };
})
.controller('characterDetailsCtrl', function ($scope, $mdDialog, character) {
    $scope.character = character;

    $scope.close = function () {
        $mdDialog.hide();
    }
});
