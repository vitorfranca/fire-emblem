app.directive('attributes', function () {
    return {
        restrict: 'AE',
        templateUrl: 'html/attributes.html',
        scope: { attributesRates: "=" },
        link: function ($scope, $elem, $attr) {
            if($attr.attributes)
                $scope.attributes = angular.fromJson($attr.attributes);
        }
    };
});
