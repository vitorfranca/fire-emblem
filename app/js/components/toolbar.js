app.directive('toolbar', function ($window, $timeout) {
	return {
		restrict: 'AE',
		templateUrl: 'html/toolbar.html',
		replace: true
	};
});
