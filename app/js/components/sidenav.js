app.directive('sidenav', function () {
	return {
		restrict: 'AE',
		templateUrl: 'html/sidenav.html',
		replace: true
	};
});
