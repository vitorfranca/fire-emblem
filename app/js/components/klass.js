app.directive('klass', function ($window, $timeout) {
    return {
        restrict: 'AE',
        templateUrl: 'html/klass.html',
        scope: {},
        link: function ($scope, $elem, $attr) {
            $scope.klass = angular.fromJson($attr.klass);
        },
        controller: 'klassCtrl'
    };
})
.controller('klassCtrl', function ($scope, $mdDialog) {
    $scope.showDetails = function(ev) {
        console.log($scope.klass);
        $mdDialog.show({
            controller: 'klassDetailsCtrl',
            templateUrl: 'html/klassDetails.html',
            targetEvent: ev,
            locals: {
                klass: $scope.klass
            }
        });
    };
})
.controller('klassDetailsCtrl', function ($scope, $mdDialog, klass) {
    $scope.klass = klass;

    $scope.close = function () {
        $mdDialog.hide();
    }
});
