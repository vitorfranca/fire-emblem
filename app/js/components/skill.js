app.directive('skill', function ($window, $timeout) {
    return {
        restrict: 'AE',
        templateUrl: 'html/skill.html',
        scope: {},
        link: function ($scope, $elem, $attr) {
            $scope.skill = angular.fromJson($attr.skill);
        },
        controller: 'skillCtrl'
    };
})
.controller('skillCtrl', function ($scope, $mdDialog) {
    $scope.showDetails = function(ev) {
        console.log($scope.skill);
        $mdDialog.show({
            controller: 'skillDetailsCtrl',
            templateUrl: 'html/skillDetails.html',
            targetEvent: ev,
            locals: {
                skill: $scope.skill
            }
        });
    };
})
.controller('skillDetailsCtrl', function ($scope, $mdDialog, skill) {
    $scope.skill = skill;

    $scope.close = function () {
        $mdDialog.hide();
    }
});
