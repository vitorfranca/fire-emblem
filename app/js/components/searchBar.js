app.directive('searchBar', function ($window, $timeout) {
    return {
        restrict: 'AE',
        templateUrl: 'html/searchBar.html',
        scope: { ngModel: "=" }
    };
})
