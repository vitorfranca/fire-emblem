var app = angular.module('FireEmblem', [
    'ngMaterial',
    'ui.router'
]);

app.config(function ($locationProvider, $stateProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider.state('home', {
        url: '',
        template: '<ui-view/>',
        controller: function ($scope, Characters) {
            $scope.getCharacter = function (name) {
                return _(Characters).find({ name: name });
            };
        }
    });
});

app.controller("AppCtrl", function ($scope, $mdSidenav) {
    $scope.toggleMenu = function() {
        $mdSidenav('menu').toggle();
        $scope.search = {};
    };
});
