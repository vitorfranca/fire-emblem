app.config(function($mdThemingProvider) {
    $mdThemingProvider.alwaysWatchTheme(true);

    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('orange')
        .warnPalette('red')
        .backgroundPalette('grey', {
            'default': '100'
        });
    $mdThemingProvider.setDefaultTheme('default');
});
