app.directive('rotate', function ($window) {
	return {
		restrict: 'A',
		link: function ($scope, $elem, $attr) {
			$window.addEventListener('deviceorientation', orientationChange, false);

			function orientationChange (e) {
				$elem.css('transform', 'rotateZ(' + -(e.gamma/3) + 'deg)');
				// $elem.css('transform', 'rotateZ(' + -(e.gamma) + 'deg) rotateX(' + -(e.beta) + 'deg)');
			};
		}
	};
});
