app.filter('humanizeAttr', function () {
    return function (input) {
        var attrMap = {
            "hp": "HP",
            "str": "Strength",  "mag": "Magic",
            "skl": "Skill",     "spd": "Speed",
            "def": "Defense",   "res": "Resistance",
            "lck": "Luck",      "mov": "Movement"
        }

        return attrMap[input];
    }
})
.filter('humanizeShortAttr', function () {
    return function (input) {
        var attrMap = {
            "hp": "HP",
            "str": "Str", "mag": "Mag",
            "skl": "Skl", "spd": "Spd",
            "def": "Def", "res": "Res",
            "lck": "Lck", "mov": "Mov"
        }

        return attrMap[input];
    }
});
