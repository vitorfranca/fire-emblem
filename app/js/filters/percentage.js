app.filter('percentage', function ($filter) {
    return function (input) {
        return $filter('number')(input * 100, 0) + '%';
    };
});
