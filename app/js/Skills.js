app.factory('Skills', function () {
    return [
        {
            "name": "Dual Strike+",
            "effect": "Adds 10% to the Dual Strike rate",
            "klass": "Lord",
            "level": 1
        },
        {
            "name": "Charm",
            "effect": "Hit rate and Avoid +5 to all allies within a 3 tile radius",
            "klass": "Lord",
            "level": 10
        },
        {
            "name": "Aether",
            "effect": "Attack twice consecutively, with the first strike having a Sol effect and the second strike having a Luna effect",
            "klass": "Great Lord",
            "level": 5,
            "activation": { "stat": "skl", "chance": 0.5 }
        },
        {
            "name": "Rightful King",
            "effect": "Adds 10% to Skill activation rates",
            "klass": "Great Lord",
            "level": 15
        },
        {
            "name": "Veteran",
            "effect": "Experience gain x 1.5 when paired up",
            "klass": "Tactician",
            "level": 1
        },
        {
            "name": "Solidarity",
            "effect": "Critical and Critical Avoid +10 to adjacent allies",
            "klass": "Tactician",
            "level": 10
        },
        {
            "name": "Ignis",
            "effect": "Adds (Magic)/2 to Strength when dealing physical damage and (Strength)/2 to Magic when dealing magical damage",
            "klass": "Grandmaster",
            "level": 5,
            "activation": { "stat": "skl", "chance": 1 }
        },
        {
            "name": "Rally Spectrum",
            "effect": "All stats +4 to all allies within a 3 tile radius for one Turn when the Rally command is used",
            "klass": "Grandmaster",
            "level": 15
        },
        {
            "name": "Discipline",
            "effect": "Weapon experience x2",
            "klass": "Cavalier",
            "level": 1
        },
        {
            "name": "Outdoor Fighter",
            "effect": "Hit rate and Avoid +10 when fighting outdoors",
            "klass": "Cavalier",
            "level": 10
        },
        {
            "name": "Skill +2",
            "effect": "Skill +2",
            "klass": "Archer",
            "level": 1
        },
        {
            "name": "Prescience",
            "effect": "Hit rate and Avoid +15 during the user's turn",
            "klass": "Archer",
            "level": 10
        },
        {
            "name": "Hit Rate +20",
            "effect": "Hit Rate +20",
            "klass": "Sniper",
            "level": 5
        },
        {
            "name": "Bowfaire",
            "effect": "Strength +5 when equipped with a bow",
            "klass": "Sniper",
            "level": 15
        },
        {
            "name": "Rally Skill",
            "effect": "Skill +4 to all allies within a 3 tile radius for one turn",
            "klass": "Bow Knight",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Bowbreaker",
            "effect": "Hit rate and Avoid +50 when the enemy is equipped with a bow",
            "klass": "Bow Knight",
            "level": 15
        },
        {
            "name": "Defender",
            "effect": "All stats +1 when paired up",
            "klass": "Paladin",
            "level": 5
        },
        {
            "name": "Aegis",
            "effect": "Helves damage from bows, Tomes and dragonstones",
            "klass": "Paladin",
            "activation": { "stat": "skl", "chance": 1 },
            "level": 15
        },
        {
            "name": "Luna",
            "effect": "Ignores half of the enemy’s Defence or Resistance",
            "klass": "Great Knight",
            "activation": { "stat": "skl", "chance": 1 },
            "level": 5
        },
        {
            "name": "Dual Guard+",
            "effect": "Adds 10% to the Dual Guard rate",
            "klass": "Great Knight",
            "level": 15
        },
        {
            "name": "Rally Defence",
            "effect": "Defence +4 to all allies within a 3 tile radius for one turn",
            "klass": "General",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Pavise",
            "effect": "Halves damage from Swords, Lances, Axes (includes magical variants) and beaststones",
            "klass": "General",
            "activation": { "stat": "skl", "chance": 1 },
            "level": 15
        },
        {
            "name": "Avoid +10",
            "effect": "Avoid +10",
            "klass": "Myrmidon",
            "level": 1
        },
        {
            "name": "Vantage",
            "effect": "When HP under half, always attack first during the enemy’s turn",
            "klass": "Myrmidon",
            "level": 10
        },
        {
            "name": "Astra",
            "effect": "Deals 5 consecutive hits with half damage",
            "klass": "Swordmaster",
            "activation": { "stat": "skl", "chance": 0.5 },
            "level": 5
        },
        {
            "name": "Swordfaire",
            "effect": "Strength +5 when equipped with a Sword (Magic +5 when equipped with the Levin Sword",
            "klass": "Swordmaster",
            "level": 15
        },
        {
            "name": "Armsthrift",
            "effect": "attack does not reduce weapon usage",
            "klass": "Mercenary",
            "activation": { "stat": "lck", "chance": 2 },
            "level": 1
        },
        {
            "name": "Patience",
            "effect": "Hit rate and Avoid +10 during the enemy’s turn",
            "klass": "Mercenary",
            "level": 10
        },
        {
            "name": "Sol",
            "effect": "Recover HP equal to half the damage dealt to the enemy",
            "klass": "Hero",
            "activation": { "stat": "skl", "chance": 1 },
            "level": 5
        },
        {
            "name": "Axebreaker",
            "effect": "Hit rate and Avoid +50 when the enemy is equipped with an Axe",
            "klass": "Hero",
            "level": 15
        },
        {
            "name": "HP +5",
            "effect": "Maximum HP +5",
            "klass": "Fighter",
            "level": 1
        },
        {
            "name": "Zeal",
            "effect": "Critical +5",
            "klass": "Fighter",
            "level": 10
        },
        {
            "name": "Rally Strength",
            "effect": "Strength +4 to all allies within a 3 tile radius for one Turn",
            "klass": "Warrior",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Counter",
            "effect": "Returns damage when attacked by an adjacent enemy (except damage that KOs the user)",
            "klass": "Warrior",
            "level": 15
        },
        {
            "name": "Despoil",
            "effect": "Obtain Bullion (S) from the enemy if the user defeats the enemy",
            "klass": "Barbarian",
            "activation": { "stat": "lck", "chance": 1 },
            "level": 1
        },
        {
            "name": "Gamble",
            "effect": "Hit rate -5, Critical +10",
            "klass": "Barbarian",
            "level": 10
        },
        {
            "name": "Wrath",
            "effect": "Critical +20 when under half HP",
            "klass": "Berserker",
            "level": 5
        },
        {
            "name": "Axefaire",
            "effect": "Strength +5 when equipped with an Axe (Magic +5 when equipped with the Bolt Axe)",
            "klass": "Berserker",
            "level": 15
        },
        {
            "name": "Locktouch",
            "effect": "Open doors and chests without the need of keys",
            "klass": "Thief",
            "level": 1
        },
        {
            "name": "Movement +1",
            "effect": "Movement +1",
            "klass": "Thief",
            "level": 10
        },
        {
            "name": "Lethality",
            "effect": "Instantly defeats the enemy",
            "klass": "Assassin",
            "activation": { "stat": "skl", "chance": 0.25 },
            "level": 5
        },
        {
            "name": "Pass",
            "effect": "User can pass through tiles occupied by enemy units",
            "klass": "Assassin",
            "level": 15
        },
        {
            "name": "Lucky Seven",
            "effect": "Hit rate and Avoid +20 up to the 7th Turn",
            "klass": "Trickster",
            "level": 5
        },
        {
            "name": "Acrobat",
            "effect": "All traversable terrain costs 1 movement point to cross",
            "klass": "Trickster",
            "level": 15
        },
        {
            "name": "Speed +2",
            "effect": "Speed +2",
            "klass": "Pegasus Knight",
            "level": 1
        },
        {
            "name": "Relief",
            "effect": "Recover 20% HP at the start of the user’s Turn if no units are within a 3 tile radius",
            "klass": "Pegasus Knight",
            "level": 5
        },
        {
            "name": "Rally Speed",
            "effect": "Speed +4 to all allies within a 3 tile radius for one Turn",
            "klass": "Falcon Knight",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Lancefaire",
            "effect": "Strength +5 when equipped with a Lance (Magic +5 when equipped with the Shockstick)",
            "klass": "Falcon Knight",
            "level": 15
        },
        {
            "name": "Rally Movement",
            "effect": "Movement +1 to all allies within a 3 tile radius for one Turn",
            "klass": "Dark Flier",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Galeforce",
            "effect": "Allows the user another full action after they defeat an enemy during the user’s Turn (only once per Turn)",
            "klass": "Dark Flier",
            "level": 15
        },
        {
            "name": "Strength +2",
            "effect": "Strength +2",
            "klass": "Wyvern Rider",
            "level": 1
        },
        {
            "name": "Tantivy",
            "effect": "Hit rate and Avoid +10 if no allies are within a 3 tile radius",
            "klass": "Wyvern Rider",
            "level": 10
        },
        {
            "name": "Quick Burn",
            "effect": "Hit rate and Avoid +15 at the start of the chapter. Effect decreases with each pass Turn",
            "klass": "Wyvern Lord",
            "level": 5
        },
        {
            "name": "Swordbreaker",
            "effect": "Hit rate and Avoid +50 when the enemy is equipped with a Sword",
            "klass": "Wyvern Lord",
            "level": 15
        },
        {
            "name": "Deliverer",
            "effect": "Movement +2 when paired up",
            "klass": "Griffon Rider",
            "level": 5
        },
        {
            "name": "Lancebreaker",
            "effect": "Hit rate and Avoid +50 when the enemy is equipped with a Lance",
            "klass": "Griffon Rider",
            "level": 15
        },
        {
            "name": "Magic +2",
            "effect": "Magic +2",
            "klass": "Mage",
            "level": 1
        },
        {
            "name": "Focus",
            "effect": "Critical +10 when no allies within a 3 tile radius",
            "klass": "Mage",
            "level": 10
        },
        {
            "name": "Rally Magic",
            "effect": "Magic +4 to all allies within a 3 tile radius for one Turn",
            "klass": "Sage",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Tomefaire",
            "effect": "Magic +5 when equipped with a Tome",
            "klass": "Sage",
            "level": 15
        },
        {
            "name": "Hex",
            "effect": "Avoid -15 to all adjacent enemies",
            "klass": "Dark Mage",
            "level": 1
        },
        {
            "name": "Anathema",
            "effect": "Avoid and Critical Avoid -10 to all enemies within a 3 tile radius",
            "klass": "Dark Mage",
            "level": 10
        },
        {
            "name": "Vengeance",
            "effect": "Deals (user’s max HP - Current HP)/2 extra damage",
            "klass": "Sorcerer",
            "activation": { "stat": "skl", "chance": 2 },
            "level": 5
        },
        {
            "name": "Tomebreaker",
            "effect": "Hit rate and Avoid +50 when the enemy is equipped with a Tome",
            "klass": "Sorcerer",
            "level": 15
        },
        {
            "name": "Slowburn",
            "effect": "Hit rate and Avoid increases by 1 each Turn, up to the 15th Turn",
            "klass": "Dark Knight",
            "level": 5
        },
        {
            "name": "Lifetaker",
            "effect": "User recovers 50% HP after they defeat an enemy during the user’s Turn",
            "klass": "Dark Knight",
            "level": 15
        },
        {
            "name": "Resistance +2",
            "effect": "Resistance +2",
            "klass": "Troubadour",
            "level": 1
        },
        {
            "name": "Demoiselle",
            "effect": "Avoid and Critical Avoid +10 to all male allies within a 3 tile radius",
            "klass": "Troubadour",
            "level": 10
        },
        {
            "name": "Rally Resistance",
            "effect": "Resistance +4 to all allies within a 3 tile radius for one Turn",
            "klass": "Valkyrie",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Dual Support+",
            "effect": "Increases the support bonus effect",
            "klass": "Valkyrie",
            "level": 15
        },
        {
            "name": "Aptitude",
            "effect": "Adds 20% to all growth rates during Level Ups",
            "klass": "Villager",
            "level": 1
        },
        {
            "name": "Underdog",
            "effect": "Hit rate and Avoid +15 when user’s Level is lower than the enemy’s (promoted units count as Level +20)",
            "klass": "Villager",
            "level": 15
        },
        {
            "name": "Luck +4",
            "effect": "Luck +4",
            "klass": "Dancer",
            "level": 1
        },
        {
            "name": "Special Dance",
            "effect": "Strength, Magic, Defence and Resistance +2 for one Turn for the unit who receives the user’s Dance",
            "klass": "Dancer",
            "level": 15
        },
        {
            "name": "Even Rhythm",
            "effect": "Hit rate and Avoid +10 during even numbered Turns",
            "klass": "Taguel",
            "level": 1
        },
        {
            "name": "Beastbane",
            "effect": "Deals effective damage to Beast units when user is a Taguel",
            "klass": "Taguel",
            "level": 15
        },
        {
            "name": "Odd Rhythm",
            "effect": "Hit rate and Avoid +10 during odd numbered Turns",
            "klass": "Manakete",
            "level": 1
        },
        {
            "name": "Wyrmsbane",
            "effect": "Deals effective damage to Dragon units when user is a Manakete",
            "klass": "Manakete",
            "level": 15
        },
        {
            "name": "Miracle",
            "effect": "Character survives with 1 HP after receiving an attack that would otherwise KO them (must have over 1 HP)",
            "klass": "Cleric",
            "level": 1
        },
        {
            "name": "Healtouch",
            "effect": "Restores an extra 5 HP when healing allies",
            "klass": "Cleric",
            "level": 10
        },
        {
            "name": "Rally Luck",
            "effect": "Luck +8 to all allies within a 3 tile radius for one Turn",
            "klass": "War Cleric",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Renewal",
            "effect": "Recover 30% HP at the start of the user’s Turn",
            "klass": "War Cleric",
            "level": 15
        },
        {
            "name": "Miracle",
            "effect": "Character survives with 1 HP after receiving an attack that would otherwise KO them (must have over 1 HP)",
            "klass": "Priest",
            "level": 1
        },
        {
            "name": "Healtouch",
            "effect": "Restores an extra 5 HP when healing allies",
            "klass": "Priest",
            "level": 10
        },
        {
            "name": "Rally Luck",
            "effect": "Luck +8 to all allies within a 3 tile radius for one Turn",
            "klass": "War Monk",
            "activation": "command",
            "level": 5
        },
        {
            "name": "Renewal",
            "effect": "Recover 30% HP at the start of the user’s Turn",
            "klass": "War Monk",
            "level": 15
        },
        {
            "name": "Shadowgift",
            "effect": "Enables usage of Dark Tomes for Tome wielders"
        },
        {
            "name": "Conquest",
            "effect": "Negates user’s beast and Armour type weaknesses"
        },
        {
            "name": "Resistance +10",
            "effect": "Resistance +10",
            "klass": "Dread Fighter",
            "level": 1
        },
        {
            "name": "Aggressor",
            "effect": "Attack +10 during the user’s Turn",
            "klass": "Dread Fighter",
            "level": 15
        },
        {
            "name": "Rally Heart",
            "effect": "All stats +2 and Movement +1 to all allies within a 3 tile radius for one Turn",
            "klass": "Bride",
            "activation": "command",
            "level": 1
        },
        {
            "name": "Bond",
            "effect": "Restores 10 HP to all allies within a 3 tile radius at the beginning of the user’s Turn",
            "klass": "Bride",
            "level": 15
        },
        {
            "name": "All Stats +2",
            "effect": "Strength, Magic, Skill, Speed, Luck, Defence and Resistance +2"
        },
        {
            "name": "Paragon",
            "effect": "Experience gain x2"
        },
        {
            "name": "Iote’s Shield",
            "effect": "Negates user’s Flying type weakness"
        },
        {
            "name": "Limit Breaker",
            "effect": "Raises the character’s maximum stats by 10"
        }
    ];
});
