var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var sourcemaps  = require('gulp-sourcemaps');
var rename      = require('gulp-rename');
var clean       = require('gulp-clean');
var modRewrite  = require('connect-modrewrite');

gulp.task('clean', function () {
  gulp.src('public/js', {read: false})
    .pipe(clean());
  gulp.src('public/css', {read: false})
    .pipe(clean());
});

gulp.task('serve', ['sass'], function() {
    browserSync({
        server: {
            baseDir: ["./app", "./app/public"],
            middleware: [
                modRewrite(['!\\.\\w+$ /index.html [L]'])
            ]
        }
    });

    gulp.watch("app/scss/**/*.scss", ['sass']).on('change', reload);
    gulp.watch("app/js/**/*.js", ['js']).on('change', reload);
    gulp.watch("app/**/*.html").on('change', reload);
});

gulp.task('sass', function() {
    return gulp.src("app/scss/*.scss")
    .pipe(sass())
    .pipe(concat('app.css'))
    .pipe(gulp.dest("app/public/css"))
    .pipe(reload({stream: true}));
});

gulp.task('js', function () {
    return gulp.src(['app/js/app.js', 'app/js/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('app/public/js'))
    .pipe(reload({stream: true}));
});

gulp.task('bower', function () {
    return gulp.src(['bower_components/angular/angular.min.js', 'bower_components/**/*min.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('app/public/js'))
    .pipe(reload({stream: true}));
});
gulp.task('bowercss', function () {
    return gulp.src('bower_components/**/*.min.css')
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.min.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('app/public/css'))
    .pipe(reload({stream: true}));
});


gulp.task('build', ['clean', 'js', 'bower', 'bowercss', 'sass']);
gulp.task('default', ['build', 'serve'], reload());
